from quot import read_config, track_file
import glob

# The quot package is available from https://github.com/alecheckert/quot

basefname = "D1_mNG-EN_EFH_20nM_PA-JF646_200nM_JFX549_1ms-AOTF100-1100mW-633nm_7msCam_cell"
flist = glob.glob("%s*.nd2" % basefname)

config = read_config('settings.toml')


columns = ['y','x','I0','bg','y_err','x_err','I0_err','bg_err','H_det','error_flag', 
        'snr','rmse','n_iter','y_detect','x_detect','frame','loc_idx','trajectory',  
        'subproblem_n_traj','subproblem_n_locs']


for f in flist:
    currbasefname = f[:-4]
    trajs = track_file(f, **config)
    print(trajs)
    trajs.to_csv('%s.csv' % currbasefname, index=False, columns=columns)



