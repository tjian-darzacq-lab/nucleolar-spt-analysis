%% This script plots the output of the randomization procedure

NLrand = csvread('nucleolus_randomized.csv');
NPrand = csvread('nucleoplasm_randomized.csv');
NPreal = load('../SavedPlots/Nucleus_output.mat');
NLreal = load('../SavedPlots/Nucleolus_output.mat');


mkdir results
%% plot of free diffusion coefficient for randomized masks
n = 1;
mycyan = [0,.5,.5];

[NLn,NLx] = hist(NLrand(:,n),20);
[NPn,NPx] = hist(NPrand(:,n),20);

figure;
hold on;
stairs(NLx,NLn,'Color',mycyan,'LineWidth',2)
stairs(NPx,NPn,'Color','r','LineWidth',2)
xlabel('D_{free}')
ylabel('Number of simulations')
a = axis;
plot([1,1]*NPreal.Output_struct.merged_model_params(n),...
    a(3:4),'-','Color','r','LineWidth',2)
plot([1,1]*NPreal.Output_struct.merged_model_params(n),...
    a(3:4),'--k','LineWidth',2)
plot([1,1]*NLreal.Output_struct.merged_model_params(n),...
    a(3:4),'-','Color',mycyan,'LineWidth',2)  
plot([1,1]*NLreal.Output_struct.merged_model_params(n),...
    a(3:4),'--k','LineWidth',2)  

saveas(gcf,'results/Dfree.fig');
saveas(gcf,'results/Dfree.png');

%%
n = 2;
mycyan = [0,.5,.5];

[NLn,NLx] = hist(NLrand(:,n),20);
[NPn,NPx] = hist(NPrand(:,n),20);

figure;
hold on;
stairs(NLx,NLn,'Color',mycyan,'LineWidth',2)
stairs(NPx,NPn,'Color','r','LineWidth',2)
xlabel('D_{bound}')
ylabel('Number of simulations')
a = axis;
plot([1,1]*NPreal.Output_struct.merged_model_params(n),...
    a(3:4),'-','Color','r','LineWidth',2)
plot([1,1]*NPreal.Output_struct.merged_model_params(n),...
    a(3:4),'--k','LineWidth',2)
plot([1,1]*NLreal.Output_struct.merged_model_params(n),...
    a(3:4),'-','Color',mycyan,'LineWidth',2)  
plot([1,1]*NLreal.Output_struct.merged_model_params(n),...
    a(3:4),'--k','LineWidth',2)  

saveas(gcf,'results/Dbound.fig');
saveas(gcf,'results/Dbound.png');

%%
n = 3;
mycyan = [0,.5,.5];

[NLn,NLx] = hist(NLrand(:,n),20);
[NPn,NPx] = hist(NPrand(:,n),20);

figure;
hold on;
stairs(NLx,NLn,'Color',mycyan,'LineWidth',2)
stairs(NPx,NPn,'Color','r','LineWidth',2)
xlabel('F_{bound}')
ylabel('Number of simulations')
a = axis;
plot([1,1]*NPreal.Output_struct.merged_model_params(n),...
    a(3:4),'-','Color','r','LineWidth',2)
plot([1,1]*NPreal.Output_struct.merged_model_params(n),...
    a(3:4),'--k','LineWidth',2)
plot([1,1]*NLreal.Output_struct.merged_model_params(n),...
    a(3:4),'-','Color',mycyan,'LineWidth',2)  
plot([1,1]*NLreal.Output_struct.merged_model_params(n),...
    a(3:4),'--k','LineWidth',2)  

saveas(gcf,'results/Fbound.fig');
saveas(gcf,'results/Fbound.png');


%%
mean(NPrand)
std(NPrand)

%%
mean(NLrand)
std(NLrand)

%%
NPreal.Output_struct.merged_model_params

%%
NLreal.Output_struct.merged_model_params

