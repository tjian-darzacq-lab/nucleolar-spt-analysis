function runSpotOnForRandomized(replicateNumber,outdir)
% Run Spot-On for 

% % % % % % Make list of mat files for each compartment % % % % % % % %

workspaces = {{},{},{}}; % list of mat files for compartments 0, 1, and 2
allTrajectories = {};

cellsToInclude = [1:13,15:20]; 

for j=0:2
    for c = 1:numel(cellsToInclude) % range of cell numbers
        k = cellsToInclude(c);
        currfname = ['bootstrapReplicates' filesep num2str(replicateNumber)...
            filesep 'sortedTrajectories/' num2str(k) '/' num2str(j) '.mat'];
        if exist(currfname,'file')
            % compartment 1.5 = outside nucleus = index 1
            % compartment 1 = nucleoplasm = index 2
            % compartment 2 = nucleoli = index 3            
            workspaces{j + 1}{end+1} = currfname;
            allTrajectories{end+1} = currfname;
        end
    end
end

sampleNames = {'Cytoplasm','Nucleoplasm','Nucleolus'}; % compartment names

% Run Spot-On for compartments 2 and 3

for j=2:3 % compartment number


    %%%%%%%%%%%%%%%%%%%%%%% DEFINE DATA SET PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%
    data_struct = struct([]);
    data_struct(1).path = [pwd, filesep];
    data_struct(1).workspaces = workspaces{j};
    data_struct(1).workspaces = workspaces{j};
    data_struct(1).Include = 1:numel(workspaces{j});
    
    Params = setSpotOnParams(data_struct);
    Params.SampleName = sampleNames{j};

    try
    [Output_struct] = SpotOn_core(Params);
    catch; end

    save([outdir filesep Params.SampleName '_output.mat'],'Output_struct');
    csvwrite([outdir filesep Params.SampleName '_params.csv'],...
        Output_struct.merged_model_params)

end

end
