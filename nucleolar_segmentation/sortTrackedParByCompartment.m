function rv = sortTrackedParByCompartment(inputTrackedPar,outfname)
% sortedTP = sortTrackedParbyCompartment(inputTrackedPar,outfname)
% 
% Divide a inputTrackedPar data structure with compartment labels into separate
% inputTrackedPar data structures, each containing trajectories within one of
% the labeled compartments. There can be an arbitrary number of different
% compartment labels.
% 
% inputs:
% inputTrackedPar - structure array with elements xy, Frame, Timestamp, and
% % Compartments
% outfname - base file name for writing out separate .mat files for each
% % compartment label
% 
% output:
% sortedTP - cell array of 1x2 cell arrays containing:
% % 1) inputTrackedPar structure with elements xy, Frame, Timestamp for
% % trajectories within a given compartment
% % 2) label corresponding to that inputTrackedPar structure
% 
% Thomas Graham, Tjian-Darzacq lab, 8-23-2020

compartmentList = [];
sortedTP = {};
for j = 1:numel(inputTrackedPar)
    currc = inputTrackedPar(j).Compartments;
    firstc = currc(1);
    
    % I think this will be faster than arrayfun, because it will stop as
    % soon as it encounters an element not identical to the first.
    allIdentical = 1;
    if numel(currc) > 1
        for k=2:numel(currc)
            if firstc ~= currc(k)
                allIdentical = 0;
                break;
            end
        end
    end
    
    % if the localizations in this trajectory are not all in the same
    % compartment, then move on to the next trajectory
    if allIdentical == 0
        continue;
    end
    
    % If there are no entries in the compartment list, then add the first
    % one
    if isempty(compartmentList)
        compartmentList(1) = firstc;
        sortedTP{1} = inputTrackedPar(j); % initialize a new structure array for trajectories
    else
        % find the appropriate compartment index, if it's there
        index = find(firstc==compartmentList,1); 
        % if the compartment index is not in the list, add it, and create a
        % new element of the sortedTP cell array
        if isempty(index)
            compartmentList(end+1) = firstc;
            sortedTP{end+1} = inputTrackedPar(j); % initialize a new structure array for trajectories
        % if the compartment is there, then append the trajectory to the
        % existing structure array
        else
            sortedTP{index}(end+1) = inputTrackedPar(j);
        end
    end
end

[~,compartmentSortIndex] = sort(compartmentList);

% to make the output a bit neater, sort the labels numerically
rv = {};
for j = 1:numel(compartmentSortIndex)
    currIndex = compartmentSortIndex(j);
    rv{end+1} = {sortedTP{currIndex},compartmentList(currIndex)};
end

% write out trajectories in each compartment as separate .mat files, named
% according to the corresponding compartment indices
for j=1:numel(rv)
    trackedPar = rv{j}{1};
    save([outfname num2str(rv{j}{2}) '.mat'], 'trackedPar');
end

end