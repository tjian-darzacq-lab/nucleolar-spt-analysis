%% read in sample nd2 file, and segment nucleus and nucleoli

fname = 'sample_image.nd2';

im = nd2read1chan_MFM(fname);

wNucleoli = 1.5;
wNucleus = 5;
nuclearThresh = 0.15;
nucleolarThresh = 0.45; 
visualizationOn = 1;

figure;
[nuclearMask,nucleolarMask] = thresholdNucleusAndNucleoli(im,...
        wNucleus, wNucleoli, nuclearThresh,nucleolarThresh,visualizationOn);
    
    
%% randomize nucleolar masks
mkdir maskRandomizationSampleOutput
nTimeSteps = 10000;
writeIncrement = 100;
displayOn = 1;
displayIncrement = 10;
teleportationFrequency = 0.01;
outfname = 'maskRandomizationSampleOutput/';


sumOfMasks = randomizeNucleolarMask(nuclearMask,nucleolarMask,outfname,...
    nTimeSteps,writeIncrement,'displayOn',displayOn,...
    'displayIncrement',displayIncrement,...
    'teleportationFrequency', teleportationFrequency);

figure;
imagesc(sumOfMasks)


%% browse through individual output files
figure;
for j=100:100:9900
    load(['maskRandomizationSampleOutput/' num2str(j) '.mat'])
    imagesc(mockNucleolarMask); axis off;
    pause(0.1)
end


