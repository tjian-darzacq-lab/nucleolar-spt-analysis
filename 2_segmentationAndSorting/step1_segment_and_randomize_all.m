addpath ../nucleolar_segmentation

%% read in nd2 files, and segment nuclei and and nucleoli

% Parameters for nucleolar segmentation
wNucleoli = 1.5;         % width of Gaussian filter for segmenting nucleoli
wNucleus = 5;            % width of Gaussian filter for segmenting nuclei
nuclearThresh = 0.05;    % relative intensity threshold for segmenting nuclei
nucleolarThresh = 0.3;   % relative intensity threshold for segmenting nucleoli 
visualizationOn = 0;     % whether or not to visualize output

figure; 
mkdir realMasks

for j=1:20
    fname1 = sprintf('../D1_mNG-EN_EFH_20nM_PA-JF646_200nM_JFX549_cell%02d_488nm_500ms_beforeSPT.nd2',j);
    fname2 = sprintf('../D1_mNG-EN_EFH_20nM_PA-JF646_200nM_JFX549_cell%02d_488nm_500ms_afterSPT.nd2',j);
    
    if exist(fname1,'file') && exist(fname2,'file')
        
        im = double(nd2read1chan_MFM(fname1));
        im = im + double(nd2read1chan_MFM(fname2));
    
        [nuclearMask,nucleolarMask] = thresholdNucleusAndNucleoli(im,...
                wNucleus, wNucleoli, nuclearThresh,nucleolarThresh,visualizationOn);
            
        save(['realMasks' filesep 'realMasks_' num2str(j) '.mat'], ...
            'nuclearMask','nucleolarMask')
    end
    
end
close all;
%% randomize nucleolar masks


% Parameters for randomization
nTimeSteps = 10000;                 % number of steps to simulate
writeIncrement = 100;               % write out after this many steps
displayOn = 0;                      % whether to display output or not
displayIncrement = 100;             % display at this frame interval
teleportationFrequency = 0.01;      % rate at which puncta are "teleported" to a new location

mkdir randMasks

for j=1:20   
    try
        load(['realMasks' filesep 'realMasks_' num2str(j) '.mat']);
        disp(['Analyzing movie ' num2str(j) '.'])
        outfname = ['randMasks/' num2str(j) filesep];
        mkdir(outfname)

        sumOfMasks = randomizeNucleolarMask(nuclearMask,nucleolarMask,outfname,...
            nTimeSteps,writeIncrement,'displayOn',displayOn,...
            'displayIncrement',displayIncrement,...
            'teleportationFrequency', teleportationFrequency);

        close all;
    catch
        disp(['Failed for ' num2str(j)])
    end
end
